import React from "react"

export default class ToDoList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            items: this.props.items || []
        }
    }

    render(){
        const {items} = this.state
        let list = []
        for(const item of items){
            list.push(<li>{item}</li>)
        }

        if(list.length == 0) return (<p>There are no to-do items!</p>)
        return (
            <ul>
                {list}
            </ul>
        )
    }
    
}
